$(function() {

		$(".menu_mobile_btn").click(function(){
		$(this).toggleClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});

		$(".menu_mobile_overlay").click(function(){
		$(".menu_mobile_btn").removeClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
});

var video;

window.onload = function() {
	video = document.getElementById("video");
};

function play() {
	video.play();
	$(".play_btn").attr('onclick','pause()');
	$(".play_btn").css({"opacity": "0"})
}
function pause() {
	video.pause();
	$(".play_btn").attr('onclick','play()');
	$(".play_btn").css({"opacity": "1"})
}




$(document).ready(function() {

	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 117;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
		e.preventDefault();
	});



	$("h2").animated("fadeInDown");
	$("h1").animated("fadeInDown");
	$(".park").animated("fadeInUp");
	$(".direction:even").animated("fadeInUp");
	$(".direction:odd").animated("fadeInUp");

	$('.park_slider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});


	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav: true,
		dots: true,
		autoplay:false,
		autoplayTimeout:4000,
		navText: ["",""],
		responsive:{
			0:{
				items:1
			}
		}
	})


	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
      	player = new YT.Player('player', {
      		height: '360',
      		width: '640',
      		videoId: 'vTw4RtV-hFI',
      		events: {
      			/*'onReady': onPlayerReady,*/
      			'onStateChange': onPlayerStateChange
      		}
      	});
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
      	event.target.playVideo();
      }

      function playYoutubeVideo() {
      	player.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
      	if (event.data == YT.PlayerState.PLAYING && !done) {
      		done = true;
      		$(".play_btn").fadeToggle(300);
      	}
      }
      function stopVideo() {
      	player.stopVideo();
      	$(".play_btn").fadeIn(300);
      }

      $(".owl-theme .owl-nav .owl-next").mousedown( function(){
      	$(this).css("background","url(../img/slider_arr_left_onclick.png)");
      });

		$(function() {
			$(window).scroll(function() {
				if($(this).scrollTop() != 0 && $(window).width()>580) {
					$('#toTop').fadeIn();
				} else {
					$('#toTop').fadeOut();
				}
			});
			$('#toTop,.logo').click(function() {
				$('body,html').animate({scrollTop:0},800);
			});
		});
