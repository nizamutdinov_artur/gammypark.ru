$(function() {

		$(".menu_mobile_btn").click(function(){
		$(this).toggleClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});

		$(".menu_mobile_overlay").click(function(){
		$(".menu_mobile_btn").removeClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
});

var video;

window.onload = function() {
	video = document.getElementById("video");
};

function play() {
	video.play();
	$(".play_btn").attr('onclick','pause()');
	$(".play_btn").css({"opacity": "0"})
}
function pause() {
	video.pause();
	$(".play_btn").attr('onclick','play()');
	$(".play_btn").css({"opacity": "1"})
}




$(document).ready(function() {

    $(".preloader").fadeOut("slow");
    
	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 117;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
		e.preventDefault();
	});



	$("h2").animated("fadeInDown");
	$("h1").animated("fadeInDown");
	$(".park").animated("fadeInUp");
	$(".direction:even").animated("fadeInUp");
	$(".direction:odd").animated("fadeInUp");

	$('.park_slider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});


	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav: true,
		dots: false,
		autoplay:true,
		autoplayTimeout:4000,
		navText: ["",""],
		responsive:{
			0:{
				items:2
			},
			768:{
				items:3
			},
			1200:{
				items:4
			}
		}
	})


	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      // 3. This function creates an <iframe> (and YouTube player)
      //    after the API code downloads.
      var player;
      function onYouTubeIframeAPIReady() {
      	player = new YT.Player('player', {
      		height: '360',
      		width: '640',
      		videoId: 'KrB0mVJR4PU',
      		events: {
      			/*'onReady': onPlayerReady,*/
      			'onStateChange': onPlayerStateChange
      		}
      	});
      }

      // 4. The API will call this function when the video player is ready.
      function onPlayerReady(event) {
      	event.target.playVideo();
      }

      function playYoutubeVideo() {
      	player.playVideo();
      }

      // 5. The API calls this function when the player's state changes.
      //    The function indicates that when playing a video (state=1),
      //    the player should play for six seconds and then stop.
      var done = false;
      function onPlayerStateChange(event) {
      	if (event.data == YT.PlayerState.PLAYING && !done) {
      		done = true;          
      		$(".play_btn").fadeToggle(300);
      	}
      }
      function stopVideo() {
      	player.stopVideo();
      	$(".play_btn").fadeIn(300);
      }

      $(".owl-theme .owl-nav .owl-next").mousedown( function(){
      	$(this).css("background","url(../img/slider_arr_left_onclick.png)");
      });

		$(function() {
			$(window).scroll(function() {
				if($(this).scrollTop() != 0 && $(window).width()>580) {
					$('#toTop').fadeIn();
				} else {
					$('#toTop').fadeOut();
				}
			});
			$('#toTop,.logo').click(function() {
				$('body,html').animate({scrollTop:0},800);
			});
		});


$(".panel-heading").click(function(){
	$(this).toggleClass("active").next().slideToggle();
});



// Определяем переменную map
var map1;
var map2;
var map3;

// Функция initMap которая отрисует карту на странице
function initMap() {
// Snazzy Map Style


// В переменной map создаем объект карты GoogleMaps и вешаем эту переменную на <div id="map"></div>
var map1 = new google.maps.Map($('#map')[0], {
	zoom: 16,
	center: new google.maps.LatLng(54.738520, 56.025915),
	scrollwheel: false
});

var map2 = new google.maps.Map($('#map2')[0], {
	zoom: 16,
	center: new google.maps.LatLng(54.774555, 56.023573),
	scrollwheel: false
});

var map3 = new google.maps.Map($('#map3')[0], {
	zoom: 16,
	center: new google.maps.LatLng(54.740890, 55.946575),
	scrollwheel: false
});

// Add a marker
var marker1 = new google.maps.Marker({
	map: map1,
	position: new google.maps.LatLng(54.738679, 56.029839),
	icon: '../img/map_icon.png'
});
var marker2 = new google.maps.Marker({
	map: map2,
	position: new google.maps.LatLng(54.774376, 56.020361),
	icon: '../img/map_icon.png'
});
var marker3 = new google.maps.Marker({
	map: map3,
	position: new google.maps.LatLng(54.740795, 55.949799),
	icon: '../img/map_icon.png'
});
}

//end map1




