$(function() {

	$(".menu_mobile_btn").click(function(){
		$(this).toggleClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});

		$(".menu_mobile_overlay").click(function(){
		$(".menu_mobile_btn").removeClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
});





$(document).ready(function() {
    $(".preloader").fadeOut("slow");
    
	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 117;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
		e.preventDefault();
	});



	$("h2").animated("fadeInDown");
	$("h1").animated("fadeInDown");
	$(".park").animated("fadeInUp");
	$(".direction:even").animated("fadeInUp");
	$(".direction:odd").animated("fadeInUp");

	$('.park_slider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});


	$('.hap_cl_slider').owlCarousel({
		loop:true,
		margin:10,
		nav: true,
		dots: true,
		autoplay:true,
		autoplayTimeout:4000,
		navText: ["",""],
		autoHeight: true,
		responsive:{
			0:{
				items:1,
				nav: false
			},
			1200:{
				items:1
			}
		}
	})

	$('.hlop_slider').owlCarousel({
		loop:true,
		nav: true,
		dots: false,
		navText: ["",""],
		mouseDrag: false,
		touchDrag: false,
    URLhashListener:true,
    startPosition: 'URLHash',
		responsive:{
			0:{
				items:1,
				nav: false
			},
			1200:{
				items:1
			}
		}
	})

	$('.hlop_slider_link').click( function(){
		$('.hlop_slider_link').removeClass("active");
		$(this).addClass("active");
	});

	$('.hlop_slider .owl-next').click( function(){
		if ($('.hlop_slider_link.active').index() == 4){
				$('.hlop_slider_link.active').removeClass("active");
				$('.hlop_slider_link:first-child').addClass("active");

		}
  else{
		$('.hlop_slider_link.active').removeClass("active").next().addClass("active");
  }

	});

	$('.hlop_slider .owl-prev').click( function(){
				if ($('.hlop_slider_link.active').index() == 0){
				$('.hlop_slider_link.active').removeClass("active");
				$('.hlop_slider_link:last-child').addClass("active");

		}
  else{
  	$('.hlop_slider_link.active').removeClass("active").prev().addClass("active");
  }
		
	});

	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.

var video_pirates;
var video_west;
function onYouTubeIframeAPIReady() {

	video_pirates = new YT.Player('video_pirates', {
		height: '338',
		width: '520',
		videoId: 'jRtFEgHOykc',
		events: {
			'onStateChange': onPlayerStateChange
		}
	});
	video_west = new YT.Player('video_west', {
		height: '338',
		width: '520',
		videoId: 'DMvDpzHwqRY',
		events: {
			'onStateChange': onPlayerStateChange
		}
	});

}
// 4. The API will call this function when the video player is ready.

function playYoutubeVideoPirates() {
	video_pirates.playVideo();
	$("#player_pirates").delay(1000).fadeOut(300)
}
function playYoutubeVideoWest() {
	video_west.playVideo();
	$("#player_west").delay(1000).fadeOut(300)
}
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING && !done) {
		done = true;   
	}
}





$(".owl-theme .owl-nav .owl-next").mousedown( function(){
	$(this).css("background","url(../img/slider_arr_left_onclick.png)");
});

$(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() != 0 && $(window).width()>580) {
			$('#toTop').fadeIn();
		} else {
			$('#toTop').fadeOut();
		}
	});
	$('#toTop,.logo').click(function() {
		$('body,html').animate({scrollTop:0},800);
	});
});


$(".scenario").not(":first").hide();
$(".scenario_tab").click(function() {
	$(".scenario_tab").removeClass("sc_active").eq($(this).index()).addClass("sc_active");
	$(".scenario").hide().eq($(this).index()).fadeIn();
	// video_panda.stopVideo();
	// video_west.stopVideo();
	// video_pirates.stopVideo();
	$(".video_overlay").fadeIn(10)
}).eq(0);


$(".tb_example").not(":first").hide();
$(".tb_example_tab").click(function() {
	$(".tb_example_tab").removeClass("sc_active").eq($(this).index()).addClass("sc_active");
	$(".tb_example").hide().eq($(this).index()).fadeIn();
	$(".video_overlay").fadeIn(10)
}).eq(0);

// Определяем переменную map
var map;

// Функция initMap которая отрисует карту на странице
function initMap() {
// Snazzy Map Style


var map = new google.maps.Map($('#map')[0], {
	zoom: 17,
	center: new google.maps.LatLng(51.151833, 71.414679),
	scrollwheel: false
});



// Add a marker
var marker = new google.maps.Marker({
	map: map,
	position: new google.maps.LatLng(51.151692, 71.411745),
	icon: 'http://ufa.gammy-park.ru/holiday/img/map_icon.png'
});
}


/*Параллакс*/
$('.sect_programms').parallax({
	'elements': [
	{
		'selector': 'div.prog_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
					'invert': true
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 50,
					'multiplier': 0.06,
					'unit': '%'
				}
			}
		}
	}
	]
});

$('.sect_variations').parallax({
	'elements': [
	{
		'selector': 'div.var_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.var_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.017,
					'unit': '%'
				}
			}
		}
	},
	{
		'selector': 'div.var_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 150,
					'multiplier': 0.075,
					'invert': true
				}
			}
		}
	}
	]
});


$('.sect_organization').parallax({
	'elements': [
	{
		'selector': 'div.org_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.org_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.06
				}
			}
		}
	}

	]
});

$(window).bind('scroll',function(e){
	parallaxScroll();
});


function parallaxScroll(){
	var scrolled = $(window).scrollTop();
	$('.scen_par_1').css('top',(500-(scrolled*.35))+'px');
	$('.scen_par_2').css('top',(-700+(scrolled*0.35))+'px');
	$('.sp_prog_par_1').css('top',(2300-(scrolled*.35))+'px');
	$('.sp_prog_par_2').css('top',(-2300+(scrolled*0.4))+'px');
}
/*Параллакс конец*/