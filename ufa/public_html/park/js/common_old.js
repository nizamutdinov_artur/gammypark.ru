$(function() {

	$(".menu_mobile_btn").click(function(){
		$(this).toggleClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
		$(".menu_mobile_overlay").click(function(){
		$(".menu_mobile_btn").removeClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
});





$(document).ready(function() {

	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 117;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
		e.preventDefault();
	});


	$("h2").animated("fadeInDown");
	$("h1").animated("fadeInDown");
	$(".park").animated("fadeInUp");
	$(".direction:even").animated("fadeInUp");
	$(".direction:odd").animated("fadeInUp");

	$('.park_slider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});


	$('.promo_slider').owlCarousel({
		loop:true,
		margin:10,
		nav: true,
		dots: true,
		autoplay:false,
		autoplayTimeout:4000,
		navText: ["",""],
		autoHeight: true,
		responsive:{
			0:{
				items:1,
				nav: false
			},
			1200:{
				items:1
			}
		}
	})

	$('.instructors__slider').owlCarousel({
		
		margin:15,
		nav: true,
		dots: true,
		autoplay:false,
		autoplayTimeout:4000,
		navText: ["",""],
		autoHeight: true,
		responsive:{
			0:{
				items:1
			},
			480:{
				items:2
			},
			768:{
				items:3
			},
			1024:{
				items:4
			}
		}
	})


	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});


	$('.safety__questions-title').click(function(e){
		e.preventDefault();
		$(this).toggleClass('active').siblings('.safety__questions-dropdown').slideToggle();
		$('.safety__questions-title').not(this).removeClass('active').siblings('.safety__questions-dropdown').slideUp();
	});

	//tabs
	$('.tab-btn').click(function(e){
		e.preventDefault();
		var $tab = $(this).data("src"); 		
		$(this).addClass('active');
		$(this).closest('.tab-block').find('.tab-btn').not(this).removeClass('active');		
		$(this).closest('.tab-block').find('.tab-item').removeClass('active');      
		$($tab).addClass('active');	
	});


	//trail calculate
	$('.trails__map-calculate-item').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('active');			
		qty=$(this).parent('.trails__map-calculate-row').find('.active').length;
		qtyEl=$(this).parent('.trails__map-calculate-row').find('.trails__map-calculate-item').length;	
		summ1 = 0;
		summ2 = 0;

		$('.trails__map-pakages-item').removeClass('active')

		$('.trails__map-calculate-item.active').each(function(){
			var param = $(this).data('price');  
		    var res = param.split(",") ;
		    var res0 = parseInt(res[0],10); 
		  	var res1 = parseInt(res[1],10); 
		  	
			summ1 += res0;
			summ2 += res1;	  		
		})

		$('.trails__price').html(summ1 + ' p.');
	 	$('.trails__price-weekend').html(summ2 + ' p.');

		if (qty<1){
			$('.trails__qty').html('<span class="value">'+qty+'</span>,</span> ');
		}

		else if (qty<2){
			$('.trails__qty-discount').html(0 + ' p.');				
			$('.trails__qty').html('<span class="value">'+qty+'</span> трассу,</span> ');
		}

		else if (qty==2) {
			$('.trails__qty-discount').html(100 + ' p.');
			$('.trails__qty').html('<span class="value">'+qty+'</span> трассы,</span> ');
			$('.trails__price').html(summ1 -100 + ' p.');
	 		$('.trails__price-weekend').html(summ2 -100 + ' p.');				
		}

		else if (qty==3) {
			$('.trails__qty-discount').html(150 + ' p.');
			$('.trails__qty').html('<span class="value">'+qty+'</span> трассы,</span> ');
			$('.trails__price').html(summ1 -150 + ' p.');
	 		$('.trails__price-weekend').html(summ2 -150 + ' p.');			
		}

		else if (qty==4 && qty) {
			$('.trails__qty-discount').html(200 + ' p.');
			$('.trails__qty').html('<span class="value">'+qty+'</span> трассы,</span> ');
			$('.trails__price').html(summ1 -200 + ' p.');
	 		$('.trails__price-weekend').html(summ2 -200 + ' p.');				
		}

		else if (qty>4 && qty< qtyEl) {
			$('.trails__qty-discount').html(200 + ' p.');
			$('.trails__price').html(summ1 -200 + ' p.');
	 		$('.trails__qty').html('<span class="value">'+qty+'</span> трасс,</span> ');				
		}	

		else if (qty==qtyEl) {			
			$('.trails__price').html(1000 + ' p.')
			$('.trails__price-weekend').html(1200 + ' p.');	
			$('.trails__qty').html('<span class="value">все</span> трассы,</span> ');
			$('.trails__qty-discount').html('');		
		}
	})

	$('.trails__map-pakages-item').on('click',function(e){
		e.preventDefault();
		$('.trails__map-calculate-item').removeClass('active');
		$('.trails__map-pakages-item').not(this).removeClass('active');

		$(this).addClass('active');

		var param = $(this).data('price');  
		var title = $(this).attr('title');  
		var res = param.split(",") ;
		var res0 = parseInt(res[0],10); 
		var res1 = parseInt(res[1],10);  

		$('.trails__price').html(res0 + ' p.');
	 	$('.trails__price-weekend').html(res1 + ' p.');

	 	$('.trails__qty').html('пакет: <span class="value">'+title+'</span>,</span> ');
	});
});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
var video_pirates;
var video_west;
var video_panda;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '360',
		width: '640',
		videoId: 'X_5Ob1EF4QQ',
		events: {
			/*'onReady': onPlayerReady,*/
			'onStateChange': onPlayerStateChange
		}
	});
	video_pirates = new YT.Player('video_pirates', {
		height: '338',
		width: '520',
		videoId: 'jRtFEgHOykc',
		events: {
			'onStateChange': onPlayerStateChange
		}
	});
	video_west = new YT.Player('video_west', {
		height: '338',
		width: '520',
		videoId: 'DMvDpzHwqRY',
		events: {
			'onStateChange': onPlayerStateChange
		}
	});
		video_panda = new YT.Player('video_panda', {
		height: '338',
		width: '520',
		videoId: 'o7gZPdddV6s',
		events: {
			'onStateChange': onPlayerStateChange
		}
	});
}
// 4. The API will call this function when the video player is ready.
function playYoutubeVideo() {
	player.playVideo();
	$("#player_overlay").delay(1000).fadeOut(300)
}
function playYoutubeVideoPirates() {
	video_pirates.playVideo();
	$("#player_pirates").delay(1000).fadeOut(300)
}
function playYoutubeVideoWest() {
	video_west.playVideo();
	$("#player_west").delay(1000).fadeOut(300)
}
function playYoutubeVideoPanda() {
	video_panda.playVideo();
	$("#player_panda").delay(1000).fadeOut(300)
}
// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING && !done) {
		done = true;
	}
}





$(".owl-theme .owl-nav .owl-next").mousedown( function(){
	$(this).css("background","url(../img/slider_arr_left_onclick.png)");
});

$(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() != 0 && $(window).width()>580) {
			$('#toTop').fadeIn();
		} else {
			$('#toTop').fadeOut();
		}
	});
	$('#toTop,.logo').click(function() {
		$('body,html').animate({scrollTop:0},800);
	});
});


$(".scenario").not(":first").hide();
$(".scenario_tab").click(function() {
	$(".scenario_tab").removeClass("sc_active").eq($(this).index()).addClass("sc_active");
	$(".scenario").hide().eq($(this).index()).fadeIn();
	video_panda.stopVideo();
	video_west.stopVideo();
	video_pirates.stopVideo();
	$(".video_overlay").fadeIn(10)
}).eq(0);


// Определяем переменную map
var map;

// Функция initMap которая отрисует карту на странице
function initMap() {
// Snazzy Map Style


// В переменной map создаем объект карты GoogleMaps и вешаем эту переменную на <div id="map"></div>
var map = new google.maps.Map($('#map')[0], {
	zoom: 16,
	center: new google.maps.LatLng(54.737707, 56.031577),
	scrollwheel: false
});



// Add a marker
var marker = new google.maps.Marker({
	map: map,
	position: new google.maps.LatLng(54.738604, 56.029850),
	icon: 'http://ufa.gammy-park.ru/holiday/img/map_icon.png'
});
}


/*Параллакс*/
$('.sect_programms').parallax({
	'elements': [
	{
		'selector': 'div.prog_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
					'invert': true
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 50,
					'multiplier': 0.06,
					'unit': '%'
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.15,
					'invert': true
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_4',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.15,
				}
			}
		}
	}
	]
});

$('.sect_variations').parallax({
	'elements': [
	{
		'selector': 'div.var_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.var_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.017,
					'unit': '%'
				}
			}
		}
	},
	{
		'selector': 'div.var_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 150,
					'multiplier': 0.075,
					'invert': true
				}
			}
		}
	}
	]
});


$('.sect_organization').parallax({
	'elements': [
	{
		'selector': 'div.org_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.org_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.06
				}
			}
		}
	}

	]
});

$(window).bind('scroll',function(e){
	parallaxScroll();
});


function parallaxScroll(){
	var scrolled = $(window).scrollTop();
	$('.scen_par_1').css('top',(500-(scrolled*.35))+'px');
	$('.scen_par_2').css('top',(-700+(scrolled*0.35))+'px');
	$('.sp_prog_par_1').css('top',(2300-(scrolled*.35))+'px');
	$('.sp_prog_par_2').css('top',(-2300+(scrolled*0.4))+'px');
}
/*Параллакс конец*/
