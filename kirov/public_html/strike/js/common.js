$(function() {

	$(".menu_mobile_btn").click(function(){
		$(this).toggleClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});

		$(".menu_mobile_overlay").click(function(){
		$(".menu_mobile_btn").removeClass("active_btn_menu");
		$(".mobile_menu").toggleClass("menu_vis");
		$(".menu_mobile_overlay").toggleClass("menu_mobile_overlay_vis");
		$(".menu_mobile_btn .fa-window-close,.menu_mobile_btn .fa-bars").delay(1000).fadeToggle(0)
	});
});





$(document).ready(function() {

	$("body").on('click', '[href*="#"]', function(e){
		var fixed_offset = 117;
		$('html,body').stop().animate({ scrollTop: $(this.hash).offset().top - fixed_offset }, 1000);
		e.preventDefault();
	});



	$("h2").animated("fadeInDown");
	$("h1").animated("fadeInDown");
	$(".park").animated("fadeInUp");
	$(".direction:even").animated("fadeInUp");
	$(".direction:odd").animated("fadeInUp");

	$('.park_slider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});


	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav: true,
		dots: true,
		autoplay:true,
		autoplayTimeout:4000,
		navText: ["",""],
		autoHeight: true,
		responsive:{
			0:{
				items:1,
				nav: false
			},
			1200:{
				items:1
			}
		}
	})


	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

});



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '360',
		width: '640',
		videoId: 'MD2Uz7wiKMQ',
		events: {
			/*'onReady': onPlayerReady,*/
			'onStateChange': onPlayerStateChange
		}
	});

}
// 4. The API will call this function when the video player is ready.
function playYoutubeVideo() {
	player.playVideo();
	$("#player_overlay").delay(1000).fadeOut(300)
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING && !done) {
		done = true;   
	}
}





$(".owl-theme .owl-nav .owl-next").mousedown( function(){
	$(this).css("background","url(img/slider_arr_left_onclick.png)");
});

$(function() {
	$(window).scroll(function() {
		if($(this).scrollTop() != 0 && $(window).width()>580) {
			$('#toTop').fadeIn();
		} else {
			$('#toTop').fadeOut();
		}
	});
	$('#toTop,.logo').click(function() {
		$('body,html').animate({scrollTop:0},800);
	});
});


$(".scenario").not(":first").hide();
$(".scenario_tab_wr").click(function() {
	$(".scenario_tab_wr").removeClass("sc_active").eq($(this).index()).addClass("sc_active");
	$(".scenario").hide().eq($(this).index()).fadeIn();
	$(".video_overlay").fadeIn(10)
}).eq(0);



// Определяем переменную map
var map;

// Функция initMap которая отрисует карту на странице
function initMap() {
// Snazzy Map Style


// В переменной map создаем объект карты GoogleMaps и вешаем эту переменную на <div id="map"></div>
var map = new google.maps.Map($('#map')[0], {
	zoom: 16,
	center: new google.maps.LatLng(58.607466, 49.813900),
	scrollwheel: false
});



// Add a marker
var marker = new google.maps.Marker({
	map: map,
	position: new google.maps.LatLng(58.607458, 49.811239),
	icon: '../img/map_icon.png'
});
}


/*Параллакс*/
$('.sect_programms').parallax({
	'elements': [
	{
		'selector': 'div.prog_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
					'invert': true
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 50,
					'multiplier': 0.06,
					'unit': '%'
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.15,
					'invert': true
				}
			}
		}
	},
	{
		'selector': 'div.prog_par_4',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.15,
				}
			}
		}
	}
	]
});

$('.sect_variations').parallax({
	'elements': [
	{
		'selector': 'div.var_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.var_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.017,
					'unit': '%'
				}
			}
		}
	},
	{
		'selector': 'div.var_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 150,
					'multiplier': 0.075,
					'invert': true
				}
			}
		}
	},
		{
		'selector': 'div.var_hol_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 150,
					'multiplier': 0.075,
					'invert': true
				}
			}
		}
	}
	]
});


$('.sect_organization').parallax({
	'elements': [
	{
		'selector': 'div.org_par_1',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.05,
				}
			}
		}
	},
	{
		'selector': 'div.org_par_2',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.06
				}
			}
		}
	}

	]
});

$('.sect_all_conditions').parallax({
	'elements': [
	{
		'selector': 'div.cond_par_3',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 0,
					'multiplier': 0.1,
				}
			}
		}
	},
	{
		'selector': 'div.cond_par_4',
		'properties': {
			'x': {
				'background-position-x': {
					'initial': 150,
					'multiplier': 0.06,
					'invert': true
				}
			}
		}
	}

	]
});


$(window).bind('scroll',function(e){
	parallaxScroll();
});


function parallaxScroll(){
	var scrolled = $(window).scrollTop();
	$('.scen_par_1').css('top',(500-(scrolled*.35))+'px');
	$('.scen_par_2').css('top',(-700+(scrolled*0.35))+'px');
	$('.sp_prog_par_1').css('top',(4000-(scrolled*.35))+'px');
	$('.sp_prog_par_2').css('top',(-4000+(scrolled*0.4))+'px');
	$('.sp_addic_par_1').css('top',(-2650+(scrolled*0.3))+'px');
	$('.sp_addic_par_2').css('top',(3300-(scrolled*.35))+'px');
	$('.cond_par_1').css('top',(2300-(scrolled*.35))+'px');
	$('.cond_par_2').css('top',(-2300+(scrolled*0.4))+'px');
}
/*Параллакс конец*/